<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <source>Open directory</source>
        <translation type="vanished">Ouvrir le répertoire</translation>
    </message>
    <message>
        <source>Process directory</source>
        <translation type="vanished">Traiter le répertoire</translation>
    </message>
    <message>
        <source>Extract old BSA</source>
        <translation type="vanished">Extraire l&apos;ancien BSA</translation>
    </message>
    <message>
        <source>Delete old BSA</source>
        <translation type="vanished">Supprimer l&apos;ancien BSA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="23"/>
        <source>MainWindow</source>
        <translation>Fenêtre principale</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>Will write which files would be affected, without actually processing them</source>
        <translation>Ecrira la liste des fichiers qui seront modifiés, sans les traiter réellement</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="37"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="121"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts, renames, and backs up discovered BSAs, then repacks after optimization.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extrait, renomme, et sauvegarde les BSA trouvés, puis les recrée après l&apos;optimisation.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="124"/>
        <source>Optimize BSA</source>
        <translation>Optimiser les BSA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="140"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts TGA to DDS, unsupported texture types to supported ones, and compresses normal maps to BC7 format.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Convertit les TGA en DDS, les formats de textures non supportés, et compresse les normal maps au format BC7&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="143"/>
        <source>Optimize textures</source>
        <translation>Optimiser les textures</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="159"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lightly optimizes typically non-crashing meshes. This may fix some visual issues, but also lowers quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Optimise legèrement les meshes ne faisant pas crasher le jeu. Cela pourrait corriger quelques problèmes graphiques, mais diminue aussi leur qualité.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="162"/>
        <source>Other meshes</source>
        <translation>Autre meshes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="178"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Strongly Recommended. Attempts to convert and fix any textures that would crash the game, as some older formats are incompatible with SSE.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fortement recommandé. Essaye de convertir et de corriger les textures qui feraient crasher le jeu.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="181"/>
        <source>Nifscan -fixdds</source>
        <translation>Nifscan -fixdds</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="200"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Advanced options. They override simple options.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Options avancées. Elles remplacent les options simples.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="216"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Backs up any BSAs present in the mod folder, adding .bak to the filename.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sauvegarde les BSA présents dans le dossier du mod, en ajoutant &quot;.bak&quot; à leur nom.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="219"/>
        <source>Rename old BSA</source>
        <translation>Renommer les anciens BSA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="238"/>
        <source>Process Directory</source>
        <translation>Traiter le répertoire</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="254"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Converts animations to SSE format. If an animation is already compatible, no change will be made and an error is logged.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Convertit les animations au format SSE. Si l&apos;animation est déjà compatible, aucun changement n&apos;aura lieu et une erreur sera écrite.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="267"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extracts any BSAs present in the mod folder.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Extrait les BSA présents dans le dossier du mod.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="270"/>
        <source>Extract Old BSA</source>
        <translation>Extraire les anciens BSA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="293"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;BC7 is the most efficient compression SSE supports, reducing VRAM usage without observable quality loss. Will convert uncompressed textures to BC7.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;BC7 is the most efficient compression SSE supports, reducing VRAM usage without observable quality loss.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;BC7 est le format de compression le plus efficace que SSE supporte, réduisant l&apos;usage de VRAM sans perte de qualité visible.Cette option convertira les les textures non compressées en BC7. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="296"/>
        <source>BC7 conversion</source>
        <oldsource>BC7 nmaps conversion</oldsource>
        <translation>Compression en BC7</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="323"/>
        <source>One mod</source>
        <translation>Un seul mod</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="328"/>
        <source>Several mods (MO)</source>
        <translation>Plusieurs mods (MO)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="366"/>
        <source>Dry run</source>
        <translation>Traitement blanc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="382"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Strongly Recommended. Attempts to repair which are guaranteed to crash the game. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fortement recommandé. Essaye de réparer les meshes qui feraient crasher le jeu. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="385"/>
        <source>Hard crashing meshes</source>
        <translation>Meshes faisant crasher le jeu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="401"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Runs NIF Optimizer, repairing crashing meshes and optimizing others.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Lance l&apos;optimisateur de NIF, réparant les meshes faisant crasher le jeu et optimisant les autres.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="404"/>
        <source>Optimize meshes</source>
        <translation>Optimiser les meshes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="423"/>
        <source>Open Directory</source>
        <translation>Ouvrir le répertoire</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Creates a new BSA after processing the previously extracted assets.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Crée un nouveau BSA après avoir traité les ressources précedemment extraites.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="442"/>
        <source>Create new BSA</source>
        <translation>Créer un nouveau BSA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="461"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Simple options&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; text-decoration: underline;&quot;&gt;Options simples&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="477"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Recommended. Converts any TGA files into DDS, as SSE cannot read these.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Recommandé. Convertit les fichiers TGA en DDS, car SSE ne peut pas les lire.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="480"/>
        <source>TGA conversion</source>
        <translation>Conversion des TGA</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="490"/>
        <location filename="mainwindow.ui" line="496"/>
        <source>Log</source>
        <translation>Journal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="257"/>
        <source>Optimize animations</source>
        <translation>Optimiser les animations</translation>
    </message>
    <message>
        <source>Beginning...
</source>
        <translation type="vanished">Commencement...
</translation>
    </message>
    <message>
        <source>&lt;font color=blue&gt;Completed. Please check the log to check if there have been any errors (in red) &lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=blue&gt;Complété. Veuillez vérifier si des erreurs dans le log sont présentes (en rouge)&lt;/font&gt;
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="126"/>
        <source>You have selected to perform a dry run. No files will be modified, but BSAs will be extracted if that option was selected.</source>
        <translation>Vous avez choisi de procéder à un traitement blanc. Aucun fichier ne sera modifié, mais les BSA seront extraits si l&apos;option est sélectionnée.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="141"/>
        <source>Several mods option</source>
        <translation>Option plusieurs mods</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="141"/>
        <source>You have selected the several mods option. This process may take a very long time, especially if you process BSA. The program may look frozen, but it will work.
This process has only been tested on the Mod Organizer mods folder.</source>
        <translation>Vous avez sélectionné l&apos;option plusieurs mods. Ce procédé pourrait prendre un temps très long, particulièrement si les BSA sont traités. Le programme pourrait paraitre bloqué, mais il continuera à fonctionner.
Ce procédé a seulement été testé sur le dossier mods de Mod Organizer.</translation>
    </message>
</context>
<context>
    <name>Optimiser</name>
    <message>
        <location filename="Optimiser.cpp" line="13"/>
        <location filename="Optimiser.cpp" line="133"/>
        <source>&lt;font color=Blue&gt;Beginning...&lt;/font&gt;</source>
        <translation>&lt;font color=Blue&gt;Commencement...&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="26"/>
        <source>&lt;font color=Red&gt;Havok Tool not found. Are you sure the Creation Kit is installed ? You can also put HavokBehaviorPostProcess.exe in the resources folder.&lt;/font&gt;
</source>
        <translation>&lt;font color=Red&gt;Outil Havok non trouvé. Êtes vous sûr que le Creation Kit est installé ? Vous pouvez aussi mettre le fichier HavokBehaviorPostProcess.exe  dans le dossier resources.&lt;/font&gt;
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="38"/>
        <location filename="Optimiser.cpp" line="145"/>
        <source> not found. Cancelling.</source>
        <translation> non trouvé. Annulation.</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="66"/>
        <location filename="Optimiser.cpp" line="172"/>
        <source>Current mod: </source>
        <translation>Mod actuel : </translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="120"/>
        <source>&lt;font color=blue&gt;Completed. Please read the above text to check if any errors occurred (displayed in red).&lt;/font&gt;
</source>
        <translation>&lt;font color=blue&gt;Complété. Veuillez lire le texte au dessus pour vérifier qu&apos;aucune erreur n&apos;a eu lieu (affiché en rouge)&lt;/font&gt;
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="193"/>
        <source> would be optimized by Headparts meshes option.
</source>
        <translation> serait optimisé avec l&apos;option meshes de têtes.
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="198"/>
        <source> would be optimized lightly by the Other Meshes option.
</source>
        <translation> serait légèrement optimisé avec l&apos;option autre meshes.
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="203"/>
        <source> would be optimized in full by the Hard Crashing Meshes option.
</source>
        <translation> serait complètement optimisé avec l&apos;option meshes faisant crasher le jeu.
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="219"/>
        <source> would be optimized using BC7 compression.
</source>
        <translation> serait optimisé en utilisant la compression BC7.
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="225"/>
        <source> would be converted to DDS</source>
        <translation> serait converti en DDS</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="236"/>
        <source>&lt;font color=blue&gt;Completed.&lt;/font&gt;
</source>
        <translation>&lt;font color=blue&gt;Complété.&lt;/font&gt;
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="244"/>
        <source>


&lt;font color=Blue&gt;Extracting BSA...</source>
        <translation>


&lt;font color=Blue&gt;Extraction du BSA...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="260"/>
        <source>BSA found ! Extracting...</source>
        <translation>BSA trouvé ! Extraction...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="270"/>
        <source>&lt;font color=Blue&gt;BSA successfully extracted.&lt;/font&gt;
</source>
        <translation>&lt;font color=Blue&gt;BSA extrait avec succès.&lt;/font&gt;
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="274"/>
        <source>&lt;font color=Red&gt;An error occured during the extraction. Please extract it manually. The BSA was not deleted.&lt;/font&gt;
</source>
        <translation>&lt;font color=Red&gt;Une erreur a eu lieu durant l&apos;extraction. Veuillez extraire le BSA manuellement. Le BSA n&apos;a pas été supprimé
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="287"/>
        <source>


&lt;font color=Blue&gt;Renaming BSA...</source>
        <translation>


&lt;font color=Blue&gt;Renommage du BSA...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="298"/>
        <source>BSA successfully renamed.
</source>
        <translation>BSA renommé avec succès.
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="306"/>
        <source>&lt;font color=blue&gt;


Creating a new BSA...&lt;/font&gt;</source>
        <translation>&lt;font color=blue&gt;


Création d&apos;un nouveau BSA...&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="346"/>
        <source>Compressing...(this may take a long time, do not force close the program)
</source>
        <translation>Compression...(cela pourrait prendre un temps très long, ne forcez pas l&apos;arrêt du programme)
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="354"/>
        <source>&lt;font color=Blue&gt; BSA successfully compressed.&lt;/font&gt;
</source>
        <translation>&lt;font color=Blue&gt;BSA compressé avec succès&lt;/font&gt;
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="363"/>
        <source>


&lt;font color=Blue&gt;Creating a new textures BSA...&lt;/font&gt;</source>
        <translation>


&lt;font color=Blue&gt;Création d&apos;un nouveau BSA de textures...&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="380"/>
        <source>Textures folder found. Compressing...(this may take a long time, do not force close the program)
</source>
        <translation>Dossier textures trouvé. Compression...(cela pourrait prendre un temps très long, ne forcez pas l&apos;arrêt du programme)
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="396"/>
        <source>&lt;font color=Blue&gt; Textures BSA successfully compressed.&lt;/font&gt;
</source>
        <translation>&lt;font color=Blue&gt;BSA de textures compressé avec succès.&lt;/font&gt;
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="400"/>
        <source>&lt;font color=Red&gt; An error occured during the Textures BSA compression.</source>
        <translation>&lt;font color=Red&gt; Une erreur a eu lieu durant la compression du BSA de textures..</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="425"/>
        <source>
Uncompressed texture found:
</source>
        <translation>
Texture non compressée trouvée :
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="425"/>
        <location filename="Optimiser.cpp" line="462"/>
        <source>
Compressing...</source>
        <translation>
Compression...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="434"/>
        <source>


&lt;font color=Blue&gt;Running Nifscan on the textures...</source>
        <translation>


&lt;font color=Blue&gt;Lancement des Nifscan sur les textures...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="456"/>
        <source>


&lt;font color=Blue&gt;Converting TGA files...</source>
        <translation>


&lt;font color=Blue&gt;Conversion des fichiers TGA...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="462"/>
        <source>
TGA file found: 
</source>
        <translation>
Fichier TGA trouvé : 
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="480"/>
        <source>


&lt;font color=Blue&gt;Running NifScan...</source>
        <translation>


&lt;font color=Blue&gt;Lancement de NifScan...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="576"/>
        <source>


&lt;font color=Blue&gt;Running NifOpt...</source>
        <translation>


&lt;font color=Blue&gt;Lancement de NifOpt...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="577"/>
        <source>Processing: </source>
        <translation>Traitement: </translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="620"/>
        <source>


&lt;font color=Blue&gt;Processing animations...</source>
        <translation>


&lt;font color=Blue&gt;Traitement des animations...</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="627"/>
        <source>Current file: </source>
        <translation>Fichier actuel: </translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="627"/>
        <source>
Processing...
</source>
        <translation>
Traitement...
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="639"/>
        <source>&lt;font color=Blue&gt;Animation successfully ported.&lt;/font&gt;

</source>
        <translation>&lt;font color=Blue&gt;Animation portée avec succès.&lt;/font&gt;

</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="641"/>
        <source>&lt;font color=Grey&gt; An error occured during the animation porting. Maybe it is already compatible with SSE ?&lt;/font&gt;
</source>
        <translation>&lt;font color=Grey&gt; Une erreur a eu lieu durant le portage de l&apos;animation. Peut-être est-elle déjà compatible avec SSE?&lt;/font&gt;
</translation>
    </message>
    <message>
        <location filename="Optimiser.cpp" line="666"/>
        <source>Esp found.
</source>
        <translation>Esp trouvé.
</translation>
    </message>
</context>
<context>
    <name>QPlainTextEdit</name>
    <message>
        <source>Processing BSA...
</source>
        <translation type="vanished">Traitement du BSA...
</translation>
    </message>
    <message>
        <source>BSA found ! Extracting...
</source>
        <translation type="vanished">BSA trouvé ! Extraction...
</translation>
    </message>
    <message>
        <source>&lt;font color=Blue&gt;BSA successfully extracted.&lt;/font&gt;

</source>
        <translation type="vanished">&lt;font color=Blue&gt;BSA extrait avec succès.&lt;/font&gt;

</translation>
    </message>
    <message>
        <source>BSA successfully deleted.
</source>
        <translation type="vanished">BSA supprimé avec succès.
</translation>
    </message>
    <message>
        <source>&lt;font color=Red&gt;An error occured during the extraction. Please extract it manually. The BSA was not deleted.&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Red&gt;Une erreur a eu lieu durant l&apos;extraction. Veuillez extraire le BSA manuellement. Le BSA n&apos;a pas été supprimé
</translation>
    </message>
    <message>
        <source>Processing textures...
</source>
        <translation type="vanished">Traitement des textures...
</translation>
    </message>
    <message>
        <source>Currently scanning :
</source>
        <translation type="vanished">Scan en cours :
</translation>
    </message>
    <message>
        <source>Uncompressed normal map found:
</source>
        <translation type="vanished">Normal map non compressée trouvée :
</translation>
    </message>
    <message>
        <source>
Compressing...
</source>
        <translation type="vanished">
Coimpression...
</translation>
    </message>
    <message>
        <source>TGA file found: 
</source>
        <translation type="vanished">Fichier TGA trouvé : 
</translation>
    </message>
    <message>
        <source>Processing meshes...
</source>
        <translation type="vanished">Traitement des meshes...
</translation>
    </message>
    <message>
        <source>
Processing : </source>
        <translation type="vanished">
Traitement en cours : </translation>
    </message>
    <message>
        <source>&lt;font color=&quot;Blue&quot;&gt; Mesh ported to SSE
</source>
        <translation type="vanished">&lt;font color=Blue&gt;Mesh porté sur SSE
</translation>
    </message>
    <message>
        <source>&lt;font color=&quot;Red&quot;&gt; Error while porting the mesh
</source>
        <translation type="vanished">&lt;font color=Red&gt;Erreur lors du portage du mesh
</translation>
    </message>
    <message>
        <source>&lt;font color=Blue&gt;All the meshes were processed.&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Blue&gt;Toutes les meshes ont été traités.&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>&lt;font color=&quot;Red&quot;&gt;Please ensure that NifScan.exe is located in the resources folder&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Red&gt; Veuillez vous assurer que Nifscan.exe se trouve dans le dossier resources&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>Textures folder found. Compressing...
</source>
        <translation type="vanished">Dossier textures trouvé. Compression...
</translation>
    </message>
    <message>
        <source>&lt;font color=Blue&gt; Textures BSA successfully compressed.&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Blue&gt;BSA de textures compressé avec succès.&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>Compressing...
</source>
        <translation type="vanished">Compression...
</translation>
    </message>
    <message>
        <source>&lt;font color=Blue&gt; BSA successfully compressed.&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Blue&gt;BSA compressé avec succès&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>Processing animations...</source>
        <translation type="vanished">Traitement des animations...</translation>
    </message>
    <message>
        <source>Havok tool found. Processing animations...
</source>
        <translation type="vanished">Outil Havok trouvé. Traitement des animations...
</translation>
    </message>
    <message>
        <source>&lt;font color=Blue&gt;Beginning...&lt;/font&gt;</source>
        <translation type="vanished">&lt;font color=Blue&gt;Commencement...&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Current mod: </source>
        <translation type="vanished">Mod actuel : </translation>
    </message>
    <message>
        <source>BSA found ! Extracting...</source>
        <translation type="vanished">BSA trouvé ! Extraction...</translation>
    </message>
    <message>
        <source>&lt;font color=Blue&gt;BSA successfully extracted.&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Blue&gt;BSA extrait avec succès.&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>
Uncompressed normal map found:
</source>
        <translation type="vanished">
Normal map non compressée trouvée :
</translation>
    </message>
    <message>
        <source>
Compressing...</source>
        <translation type="vanished">
Compression...</translation>
    </message>
    <message>
        <source>
TGA file found: 
</source>
        <translation type="vanished">
Fichier TGA trouvé : 
</translation>
    </message>
    <message>
        <source>&lt;font color=Red&gt;Havok Tool not found. Are you sure the Creation Kit is installed ? You can also put HavokBehaviorPostProcess.exe in the resources folder.&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Red&gt;Outil Havok non trouvé. Êtes vous sûr que le Creation Kit est installé ? Vous pouvez aussi mettre le fichier HavokBehaviorPostProcess.exe  dans le dossier resources.&lt;/font&gt;
</translation>
    </message>
    <message>
        <source> not found. Cancelling.</source>
        <translation type="vanished"> non trouvé. Annulation.</translation>
    </message>
    <message>
        <source>&lt;font color=blue&gt;Completed. Please read the above text to check if any errors occurred (displayed in red).&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=blue&gt;Complété. Veuillez lire le texte au dessus pour vérifier qu&apos;aucune erreur n&apos;a eu lieu (affiché en rouge)&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Extracting BSA...</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Extraction du BSA...</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Renaming BSA...</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Renommage du BSA...</translation>
    </message>
    <message>
        <source>BSA successfully renamed.
</source>
        <translation type="vanished">BSA renommé avec succès.
</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Converting normal maps to BC7...</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Conversion des normal maps en BC7...</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Running Nifscan on the textures...</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Lancement des Nifscan sur les textures...</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Converting TGA files...</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Conversion des fichiers TGA...</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Running NifScan...</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Lancement de NifScan...</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Running NifOpt...</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Lancement de NifOpt...</translation>
    </message>
    <message>
        <source>Processing: </source>
        <translation type="vanished">Traitement: </translation>
    </message>
    <message>
        <source>&lt;font color=blue&gt;


Creating a new BSA...&lt;/font&gt;</source>
        <translation type="vanished">&lt;font color=blue&gt;


Création d&apos;un nouveau BSA...&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Compressing...(this may take a long time, do not force close the program)
</source>
        <translation type="vanished">Compression...(cela pourrait prendre un temps très long, ne forcez pas l&apos;arrêt du programme)
</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Creating a new textures BSA...&lt;/font&gt;</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Création d&apos;un nouveau BSA de textures...&lt;/font&gt;</translation>
    </message>
    <message>
        <source>Textures folder found. Compressing...(this may take a long time, do not force close the program)
</source>
        <translation type="vanished">Dossier textures trouvé. Compression...(cela pourrait prendre un temps très long, ne forcez pas l&apos;arrêt du programme)
</translation>
    </message>
    <message>
        <source>&lt;font color=Red&gt; An error occured during the Textures BSA compression.</source>
        <translation type="vanished">&lt;font color=Red&gt; Une erreur a eu lieu durant la compression du BSA de textures..</translation>
    </message>
    <message>
        <source>


&lt;font color=Blue&gt;Processing animations...</source>
        <translation type="vanished">


&lt;font color=Blue&gt;Traitement des animations...</translation>
    </message>
    <message>
        <source>&lt;font color=Grey&gt; An error occured during the animation porting. Maybe it is already compatible with SSE ?&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Grey&gt; Une erreur a eu lieu durant le portage de l&apos;animation. Peut-être est-elle déjà compatible avec SSE?&lt;/font&gt;
</translation>
    </message>
    <message>
        <source> would be optimized by Headparts meshes option.
</source>
        <translation type="vanished"> serait optimisé avec l&apos;option meshes de têtes.
</translation>
    </message>
    <message>
        <source> would be optimized lightly by the Other Meshes option.
</source>
        <translation type="vanished"> serait légèrement optimisé avec l&apos;option autre meshes.
</translation>
    </message>
    <message>
        <source> would be optimized in full by the Hard Crashing Meshes option.
</source>
        <translation type="vanished"> serait complètement optimisé avec l&apos;option meshes faisant crasher le jeu.
</translation>
    </message>
    <message>
        <source> would be optimized using BC7 compression.
</source>
        <translation type="vanished"> serait optimisé en utilisant la compression BC7.
</translation>
    </message>
    <message>
        <source> would be converted to DDS</source>
        <translation type="vanished"> serait converti en DDS</translation>
    </message>
    <message>
        <source>&lt;font color=blue&gt;Completed.&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=blue&gt;Complété.&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>Current file: </source>
        <translation type="vanished">Fichier actuel: </translation>
    </message>
    <message>
        <source>
Processing...
</source>
        <translation type="vanished">
Traitement...
</translation>
    </message>
    <message>
        <source>&lt;font color=Blue&gt;Animation successfully ported.&lt;/font&gt;

</source>
        <translation type="vanished">&lt;font color=Blue&gt;Animation portée avec succès.&lt;/font&gt;

</translation>
    </message>
    <message>
        <source>&lt;font color=Red&gt;Havok Tool not found. Are you sure the Creation Kit is installed ?&lt;/font&gt;
</source>
        <translation type="vanished">&lt;font color=Red&gt;Outil Havok non trouvé. Ëtes vous sûr que le Creation Kit est installé ?&lt;/font&gt;
</translation>
    </message>
    <message>
        <source>Esp found.
</source>
        <translation type="vanished">Esp trouvé.
</translation>
    </message>
</context>
</TS>
